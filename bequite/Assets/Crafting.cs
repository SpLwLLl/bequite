using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crafting : MonoBehaviour
{
    public GameObject CraftingItem;
    // Start is called before the first frame updat

    // Update is called once per frame
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("pillow"))
        {
            CraftingItem.SetActive(true);
            Destroy(other);
        }
    }
}
